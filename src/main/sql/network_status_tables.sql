CREATE TABLE IF NOT EXISTS network_status(
header                              TEXT                         NOT NULL,
network_status_version              INTEGER                      NOT NULL,
vote_status                         TEXT,
consensus_method                    INTEGER,
consensus_flavor                    TEXT                         NOT NULL,
valid_after                         TIMESTAMP WITHOUT TIME ZONE,
fresh_until                         TIMESTAMP WITHOUT TIME ZONE,
valid_until                         TIMESTAMP WITHOUT TIME ZONE,
vote_seconds                        BIGINT,
dist_seconds                        BIGINT,
known_flags                         TEXT                         NOT NULL,
recommended_client_version          TEXT                         NOT NULL,
recommended_server_version          TEXT                         NOT NULL,
recommended_client_protocols        TEXT                         NOT NULL,
recommended_relay_protocols         TEXT                         NOT NULL,
required_client_protocols           TEXT                         NOT NULL,
required_relay_protocols            TEXT                         NOT NULL,
params                              TEXT                         NOT NULL,
package_lines                       TEXT                         NOT NULL,
shared_rand_previous_value          TEXT,
shared_rand_current_value           TEXT,
shared_rand_previous_num            TEXT,
shared_rand_current_num             TEXT,
dir_sources                         TEXT                         NOT NULL,
digest                              TEXT                         NOT NULL,
bandwidth_weights                   TEXT                         NOT NULL,
directory_signatures                TEXT                         NOT NULL,
PRIMARY KEY(digest));

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS network_status_totals(
  totals_id                           uuid DEFAULT uuid_generate_v4 (),
  total_consensus_weight              DOUBLE PRECISION,
  total_guard_weight                  DOUBLE PRECISION,
  total_middle_weight                 DOUBLE PRECISION,
  total_exit_weight                   DOUBLE PRECISION,
  time                                TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  network_status                      TEXT references network_status(digest),
  PRIMARY KEY(totals_id));

CREATE TABLE IF NOT EXISTS network_status_entry(
  nickname                          TEXT                         NOT NULL,
  fingerprint                       TEXT                         NOT NULL,
  digest                            TEXT                         NOT NULL,
  time                              TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  ip                                TEXT                         NOT NULL,
  or_port                           INTEGER                      NOT NULL,
  dir_port                          INTEGER                      NOT NULL,
  or_addresses                      TEXT                         NOT NULL,
  flags                             TEXT,
  version                           TEXT,
  bandwidth_unmeasured              BOOLEAN,
  bandwidth_weight                  BIGINT,
  proto                             TEXT,
  policy                            TEXT,
  port_list                         TEXT,
  flavor                            TEXT,
  microdedescriptor_digest          TEXT,
  stats                             TEXT,
  master_key_ed25519                TEXT,
  network_status                    TEXT references network_status(digest),
  PRIMARY KEY(digest));

CREATE TABLE IF NOT EXISTS network_status_entry_weights(
  weights_id                          uuid DEFAULT uuid_generate_v4 (),
  guard_weight                        DOUBLE PRECISION,
  middle_weight                       DOUBLE PRECISION,
  exit_weight                         DOUBLE PRECISION,
  guard_weight_fraction               FLOAT,
  middle_weight_fraction              FLOAT,
  exit_weight_fraction                FLOAT,
  network_weight_fraction             FLOAT,
  time                                TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  network_status_entry                TEXT references network_status_entry(digest),
  PRIMARY KEY(weights_id));

CREATE TABLE IF NOT EXISTS network_vote(
  header                              TEXT                         NOT NULL,
  network_status_version              INTEGER                      NOT NULL,
  consensus_methods                   TEXT,
  published                           TIMESTAMP WITHOUT TIME ZONE,
  valid_after                         TIMESTAMP WITHOUT TIME ZONE,
  fresh_until                         TIMESTAMP WITHOUT TIME ZONE,
  valid_until                         TIMESTAMP WITHOUT TIME ZONE,
  vote_seconds                        BIGINT,
  dist_seconds                        BIGINT,
  known_flags                         TEXT                         NOT NULL,
  recommended_client_version          TEXT                         NOT NULL,
  recommended_server_version          TEXT                         NOT NULL,
  recommended_client_protocols        TEXT                         NOT NULL,
  recommended_relay_protocols         TEXT                         NOT NULL,
  required_client_protocols           TEXT                         NOT NULL,
  required_relay_protocols            TEXT                         NOT NULL,
  params                              TEXT                         NOT NULL,
  package_lines                       TEXT                         NOT NULL,
  shared_rand_previous_value          TEXT,
  shared_rand_current_value           TEXT,
  shared_rand_previous_num            TEXT,
  shared_rand_current_num             TEXT,
  stable_uptime                       BIGINT,
  stable_mtbf                         BIGINT,
  fast_bandwidth                      BIGINT,
  guard_wfu                           DOUBLE PRECISION,
  guard_tk                            BIGINT,
  guard_bandwidth_including_exits     BIGINT,
  guard_bandwidth_excluding_exits     BIGINT,
  enough_mtbf_info                    INTEGER,
  ignoring_adv_bws                    INTEGER,
  nickname                            TEXT,
  identity                            TEXT,
  hostname                            TEXT,
  address                             TEXT,
  dir_port                            INTEGER,
  or_port                             INTEGER,
  contact                             TEXT,
  shared_rand_participate             BOOLEAN,
  shared_rand_commit_lines            TEXT,
  bandwidth_file_headers              TEXT,
  bandwidth_file_digest               TEXT,
  dir_key_cert_version                INTEGER,
  legacy_dir_key                      TEXT,
  dir_identity_key                    TEXT,
  dir_key_published                   TIMESTAMP WITHOUT TIME ZONE,
  dir_key_expires                     TIMESTAMP WITHOUT TIME ZONE,
  dir_signing_key                     TEXT,
  dir_key_cross_cert                  TEXT,
  dir_key_certification               TEXT,
  digest                              TEXT                         NOT NULL,
  directory_signatures                TEXT                         NOT NULL,
  PRIMARY KEY(digest));

CREATE TABLE IF NOT EXISTS network_vote_entry(
  nickname                          TEXT                         NOT NULL,
  fingerprint                       TEXT                         NOT NULL,
  digest                            TEXT                         NOT NULL,
  time                              TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  ip                                TEXT                         NOT NULL,
  or_port                           INTEGER                      NOT NULL,
  dir_port                          INTEGER                      NOT NULL,
  or_addresses                      TEXT                         NOT NULL,
  flags                             TEXT,
  version                           TEXT,
  bandwidth_measured                BIGINT,
  bandwidth_unmeasured               BOOLEAN,
  bandwidth_weight                  BIGINT,
  proto                             TEXT,
  policy                            TEXT,
  port_list                         TEXT,
  master_key_ed25519                TEXT,
  supported_consensus_methods       TEXT,
  network_vote                      TEXT references network_vote(digest),
  PRIMARY KEY(digest));
