package org.torproject.metrics.descriptorparser.utils;

import org.torproject.metrics.descriptorparser.utils.Gauge;
import org.torproject.metrics.descriptorparser.utils.VictoriaMetricsHttpConnectionFactory;

import io.prometheus.client.CollectorRegistry;

import io.prometheus.client.exporter.PushGateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class OpenMetricsWriter {
  private static final Logger logger = LoggerFactory.getLogger(
      OpenMetricsWriter.class);

  /**
   * Process network totals instead of node metrics.
   *
   * <p>Processed metrics have no attribute labels.
   *
   */
  public void processNetworkTotal(Gauge inprogressRequests,
      Long timestamp, double value) {
    Gauge.Child child = new Gauge.Child();
    try {
      child.setTimestampMs(timestamp);
      child.set(value);
      inprogressRequests.setChild(child);
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

  }

  /**
   * Process generic node metrics that use only fingerprint nickname and
   * (optional) node type.
   */
  public void processRequest(
      Gauge inprogressRequests, String fingerprint, String nickname,
      String node, Long timestamp, double value) {
    Gauge.Child child = new Gauge.Child();
    try {
      if (node == null) {
        inprogressRequests.labels(fingerprint, nickname);
        child.setTimestampMs(timestamp);
        child.set(value);
        inprogressRequests.setChild(child, fingerprint, nickname);
      } else {
        inprogressRequests.labels(fingerprint, nickname, node);
        child.setTimestampMs(timestamp);
        child.set(value);
        inprogressRequests.setChild(child, fingerprint, nickname, node);
      }
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

  }

  /**
   * Process dirreq requests per Country.
   */
  public void processRelayReqCountry(Gauge inprogressRequests,
      String fingerprint, String nickname, String country,
      Long timestamp, double value) {
    try {
      Gauge.Child child = new Gauge.Child();
      inprogressRequests.labels(fingerprint, nickname, country);
      child.setTimestampMs(timestamp);
      child.set(value);
      inprogressRequests.setChild(child, fingerprint, nickname, country);
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

  /**
   * Process router dirreq request per country version and transport.
   */
  public void processRouterLabelmetrics(Gauge inprogressRequests,
      String fingerprint, String nickname, String node, String country,
      String transport, String version, Long timestamp, double value) {
    Gauge.Child child = new Gauge.Child();
    try {
      if (node == null) {
        inprogressRequests.labels(fingerprint, nickname, country,
            transport, version);
        child.setTimestampMs(timestamp);
        child.set(value);
        inprogressRequests.setChild(child, fingerprint, nickname, country,
            transport, version);
      } else {
        inprogressRequests.labels(fingerprint, nickname, node, country,
            transport, version);
        child.setTimestampMs(timestamp);
        child.set(value);
        inprogressRequests.setChild(child, fingerprint, nickname, node, country,
            transport, version);
      }
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

  /**
   * Push metrics to a push gateway.
   */
  public void pushToGateway(CollectorRegistry registry) {
    PushGateway pushgateway
          = new PushGateway("172.17.0.2:8428/api/v1/import/prometheus");
    /* It is possible to add a http auth layer as:
     * pushgateway.setConnectionFactory(
         new BasicAuthHttpConnectionFactory("my_user", "my_password"));
     */
    try {
      pushgateway.setConnectionFactory(
          new VictoriaMetricsHttpConnectionFactory());
      pushgateway.pushAdd(registry, "");
    } catch (IOException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

  }

}
