package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.NetworkStatusEntry;
import org.torproject.descriptor.RelayNetworkStatusVote;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Map;

public class VoteParser {
  private static final Logger logger = LoggerFactory.getLogger(
      VoteParser.class);

  private static final String INSERT_NETWORK_VOTE_SQL
      = "INSERT INTO network_vote"
      + " (header, network_status_version, consensus_methods,"
      + " published, valid_after, fresh_until, valid_until, vote_seconds,"
      + " dist_seconds, known_flags, recommended_client_version,"
      + " recommended_server_version, recommended_client_protocols,"
      + " recommended_relay_protocols, required_client_protocols,"
      + " required_relay_protocols, params, package_lines,"
      + " shared_rand_previous_value, shared_rand_current_value,"
      + " shared_rand_previous_num, shared_rand_current_num, stable_uptime,"
      + " stable_mtbf, fast_bandwidth, guard_wfu, guard_tk,"
      + " guard_bandwidth_including_exits, guard_bandwidth_excluding_exits,"
      + " enough_mtbf_info, ignoring_adv_bws, nickname, identity, hostname,"
      + " address, dir_port, or_port, contact, shared_rand_participate,"
      + " shared_rand_commit_lines, bandwidth_file_headers,"
      + " bandwidth_file_digest, dir_key_cert_version, legacy_dir_key,"
      + " dir_identity_key, dir_key_published, dir_key_expires,"
      + " dir_signing_key, dir_key_cross_cert,"
      + " dir_key_certification, digest, directory_signatures) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?)";

  private static final String INSERT_NETWORK_VOTE_ENTRY_SQL
      = "INSERT INTO network_vote_entry"
      + " (nickname, fingerprint, digest, time, ip, or_port,"
      + " dir_port, or_addresses, flags, version, bandwidth_measured,"
      + " bandwidth_unmeasured, bandwidth_weight, proto, policy,"
      + " port_list, master_key_ed25519,"
      + " supported_consensus_methods, network_vote) VALUES "
      + "(?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?)";

  private static final long ONE_HOUR_MILLIS = 60L * 60L * 1000L;

  private static final long ONE_DAY_MILLIS = 24L * ONE_HOUR_MILLIS;

  private static final long ONE_WEEK_MILLIS = 7L * ONE_DAY_MILLIS;

  /**
   * Read votes from disk and add data to the DB.
   */
  public void run(String path, Connection conn) throws Exception {
    DescriptorUtils descUtils = new DescriptorUtils();
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor : descriptorReader.readDescriptors(
        new File(path))) {
      if (descriptor instanceof RelayNetworkStatusVote) {
        RelayNetworkStatusVote desc =
            (RelayNetworkStatusVote) descriptor;

        String digest = descUtils.calculateDigestSha256Base64(
            desc.getRawDescriptorBytes());
        this.addNetworkStatusVote(desc, digest, conn);

        for (Map.Entry<String, NetworkStatusEntry> e :
            desc.getStatusEntries().entrySet()) {
          String fingerprint = e.getKey();
          NetworkStatusEntry entry = e.getValue();

          this.addRelayStatusVote(fingerprint, entry,
              digest, conn);
        }

      } else {
        continue;
      }
    }
  }

  private void addRelayStatusVote(String fingerprint, NetworkStatusEntry entry,
      String digest, Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_NETWORK_VOTE_ENTRY_SQL);
    ) {
      preparedStatement.setString(1, entry.getNickname());
      preparedStatement.setString(2, entry.getFingerprint());
      String entryDigest = descUtils.calculateDigestSha256Base64(
          entry.getStatusEntryBytes());
      preparedStatement.setString(3, entryDigest);
      preparedStatement.setTimestamp(4,
          new Timestamp(entry.getPublishedMillis()));
      preparedStatement.setString(5, entry.getAddress());
      preparedStatement.setInt(6, entry.getOrPort());
      preparedStatement.setInt(7, entry.getDirPort());
      preparedStatement.setString(8,
          descUtils.fieldAsString(entry.getOrAddresses()));
      preparedStatement.setString(9,
          descUtils.fieldAsString(entry.getFlags()));
      preparedStatement.setString(10, entry.getVersion());
      preparedStatement.setLong(11, entry.getMeasured());
      preparedStatement.setBoolean(12, entry.getUnmeasured());
      preparedStatement.setLong(13, entry.getBandwidth());
      preparedStatement.setString(14,
          descUtils.fieldAsString(entry.getProtocols()));
      preparedStatement.setString(15, entry.getDefaultPolicy());
      preparedStatement.setString(16, entry.getPortList());
      preparedStatement.setString(17, entry.getMasterKeyEd25519());
      preparedStatement.setString(18, entry.getSupportedConsensusMethods());
      preparedStatement.setString(19, digest);
      preparedStatement.executeUpdate();

    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

  private void addNetworkStatusVote(RelayNetworkStatusVote desc, String digest,
      Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_NETWORK_VOTE_SQL);
    ) {
      preparedStatement.setString(1, "@type network-status-vote-3 1.0");
      preparedStatement.setInt(2, desc.getNetworkStatusVersion());
      preparedStatement.setString(3,
          descUtils.fieldAsString(desc.getConsensusMethods()));
      preparedStatement.setTimestamp(4,
          new Timestamp(desc.getPublishedMillis()));
      preparedStatement.setTimestamp(5,
          new Timestamp(desc.getValidAfterMillis()));
      preparedStatement.setTimestamp(6,
          new Timestamp(desc.getFreshUntilMillis()));
      preparedStatement.setTimestamp(7,
          new Timestamp(desc.getValidUntilMillis()));
      preparedStatement.setLong(8, desc.getVoteSeconds());
      preparedStatement.setLong(9, desc.getDistSeconds());
      preparedStatement.setString(10,
          descUtils.fieldAsString(desc.getKnownFlags()));
      preparedStatement.setString(11,
          String.join(", ", desc.getRecommendedClientVersions()));
      preparedStatement.setString(12,
          String.join(", ", desc.getRecommendedServerVersions()));
      preparedStatement.setString(13,
          descUtils.fieldAsString(desc.getRecommendedClientProtocols()));
      preparedStatement.setString(14,
          descUtils.fieldAsString(desc.getRecommendedRelayProtocols()));
      preparedStatement.setString(15,
          descUtils.fieldAsString(desc.getRequiredClientProtocols()));
      preparedStatement.setString(16,
          descUtils.fieldAsString(desc.getRequiredRelayProtocols()));
      preparedStatement.setString(17,
          descUtils.fieldAsString(desc.getConsensusParams()));
      preparedStatement.setString(18,
          descUtils.fieldAsString(desc.getPackageLines()));
      preparedStatement.setString(19,
          desc.getSharedRandPreviousValue());
      preparedStatement.setString(20,
          desc.getSharedRandCurrentValue());
      preparedStatement.setInt(21,
          desc.getSharedRandPreviousNumReveals());
      preparedStatement.setInt(22,
          desc.getSharedRandCurrentNumReveals());
      preparedStatement.setLong(23, desc.getStableUptime());
      preparedStatement.setLong(24, desc.getStableMtbf());
      preparedStatement.setLong(25, desc.getFastBandwidth());
      preparedStatement.setDouble(26, desc.getGuardWfu());
      preparedStatement.setLong(27, desc.getGuardTk());
      preparedStatement.setLong(28, desc.getGuardBandwidthIncludingExits());
      preparedStatement.setLong(29, desc.getGuardBandwidthExcludingExits());
      preparedStatement.setInt(30, desc.getEnoughMtbfInfo());
      preparedStatement.setInt(31, desc.getIgnoringAdvertisedBws());
      preparedStatement.setString(32, desc.getNickname());
      preparedStatement.setString(33, desc.getIdentity());
      preparedStatement.setString(34, desc.getHostname());
      preparedStatement.setString(35, desc.getAddress());
      preparedStatement.setInt(36, desc.getDirport());
      preparedStatement.setInt(37, desc.getOrport());
      preparedStatement.setString(38, desc.getContactLine());
      preparedStatement.setBoolean(39, desc.isSharedRandParticipate());
      preparedStatement.setString(40,
            String.join(", ", desc.getSharedRandCommitLines()));
      preparedStatement.setString(41, descUtils.fieldAsString(
          desc.getBandwidthFileHeaders()));
      preparedStatement.setString(42,
          desc.getBandwidthFileDigestSha256Base64());
      preparedStatement.setInt(43, desc.getDirKeyCertificateVersion());
      preparedStatement.setString(44, desc.getLegacyDirKey());
      preparedStatement.setString(45, desc.getDirIdentityKey());
      preparedStatement.setTimestamp(46,
          new Timestamp(desc.getDirKeyPublishedMillis()));
      preparedStatement.setTimestamp(47,
          new Timestamp(desc.getDirKeyExpiresMillis()));
      preparedStatement.setString(48, desc.getDirSigningKey());
      preparedStatement.setString(49, desc.getDirKeyCrosscert());
      preparedStatement.setString(50, desc.getDirKeyCertification());
      preparedStatement.setString(51, digest);
      preparedStatement.setString(52,
          descUtils.fieldAsString(desc.getSignatures()));
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
      ex.printStackTrace();
    }
  }
}
