package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.descriptor.Microdescriptor;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;

import java.util.Date;

public class MicrodescriptorParser {

  private static final String INSERT_MICRODESCRIPTOR_SQL
      = "INSERT INTO microdescriptor"
      + " (digest, time,"
      + " or_addresses, policy,"
      + " port_list, onion_key, ntor_onion_key,"
      + " family_entries, ipv6_policy, ipv6_port_list, rsa_identity,"
      + " ed25519_identity) VALUES "
      + "(?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?)";

  private static final Logger logger = LoggerFactory.getLogger(
      ServerDescriptorParser.class);

  /**
   * Parse microdescriptors and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor :
        descriptorReader.readDescriptors(new File(path))) {
      if (descriptor instanceof Microdescriptor) {
        Microdescriptor desc = (Microdescriptor) descriptor;
        this.addDescriptor(desc, conn);
      } else {
        continue;
      }
    }
  }

  private void addDescriptor(Microdescriptor desc, Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();
    try (
      PreparedStatement preparedStatement =
          conn.prepareStatement(INSERT_MICRODESCRIPTOR_SQL);
    ) {
      preparedStatement.setString(1, desc.getDigestSha256Base64());
      Date date = new Date();
      long timeMillis = date.getTime();
      preparedStatement.setTimestamp(2,
          new Timestamp(timeMillis));
      preparedStatement.setString(3,
          descUtils.fieldAsString(desc.getOrAddresses()));
      preparedStatement.setString(4, desc.getDefaultPolicy());
      preparedStatement.setString(5, desc.getPortList());
      preparedStatement.setString(6, desc.getOnionKey());
      preparedStatement.setString(7, desc.getNtorOnionKey());
      preparedStatement.setString(8, descUtils.fieldAsString(
          desc.getFamilyEntries()));
      preparedStatement.setString(9, desc.getIpv6DefaultPolicy());
      preparedStatement.setString(10, desc.getIpv6PortList());
      preparedStatement.setString(11, desc.getRsa1024Identity());
      preparedStatement.setString(12, desc.getEd25519Identity());
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }

  }
}
