package org.torproject.metrics.descriptorparser.parsers;

import org.torproject.descriptor.BridgePoolAssignment;
import org.torproject.descriptor.Descriptor;
import org.torproject.descriptor.DescriptorReader;
import org.torproject.descriptor.DescriptorSourceFactory;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.Map;

public class BridgePoolAssignmentsParser {
  private static final String INSERT_BRIDGE_POOL_ASSIGNMENTS_FILE_SQL
      = "INSERT INTO bridge_pool_assignments_file (published, header, digest)"
      + " VALUES (?, ?, ?)";

  private static final String INSERT_BRIDGE_POOL_ASSIGNMENT_SQL
      = "INSERT INTO bridge_pool_assignment (published, digest, fingerprint,"
      + " distribution_method, transports, ip, blocklist,"
      + " bridge_pool_assignments) VALUES"
      + " (?, ?, ?, ?, ?, ?, ?, ?)";

  private static final Logger logger = LoggerFactory.getLogger(
      BridgePoolAssignmentsParser.class);

  /**
   * Parse bridge pool assignments and add fields to the database.
   */
  public void run(String path, Connection conn) throws Exception {
    DescriptorUtils descUtils = new DescriptorUtils();

    // Read descriptors from disk.
    DescriptorReader descriptorReader =
        DescriptorSourceFactory.createDescriptorReader();

    for (Descriptor descriptor : descriptorReader.readDescriptors(
        new File(path))) {
      if (descriptor instanceof BridgePoolAssignment) {
        BridgePoolAssignment desc = (BridgePoolAssignment) descriptor;
        String digest = descUtils.calculateDigestSha256Base64(
            desc.getRawDescriptorBytes());

        this.addPoolAssignments(desc, digest, conn);

        for (Map.Entry<String, String> e :
            desc.getEntries().entrySet()) {
          String fingerprint = e.getKey();
          String assignment = e.getValue();
          this.addPoolAssignment(fingerprint, assignment,
              desc.getPublishedMillis(), digest, conn);
        }

      } else {
        continue;
      }
    }
  }

  private void addPoolAssignment(String fingerprint, String assignment,
      long timestamp, String digest, Connection conn) {
    DescriptorUtils descUtils = new DescriptorUtils();
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_BRIDGE_POOL_ASSIGNMENT_SQL);
    ) {
      preparedStatement.setTimestamp(1,
          new Timestamp(timestamp));
      preparedStatement.setString(2, descUtils.calculateDigestSha256Base64(
          assignment.getBytes()));
      preparedStatement.setString(3, fingerprint);
      String bridgedbDistributor = assignment.split(" ")[0];
      preparedStatement.setString(4, bridgedbDistributor);
      String[] transportsParts = assignment.split("transports=");
      if (transportsParts != null && transportsParts.length > 1) {
        preparedStatement.setString(5, transportsParts[1]);
      } else {
        preparedStatement.setString(5, "");
      }
      String[] ipParts = assignment.split("ip=");
      if (ipParts != null && ipParts.length > 1) {
        preparedStatement.setString(6, ipParts[1]);
      } else {
        preparedStatement.setString(6, "");
      }
      String[] blocklistParts = assignment.split("blocklist=");
      if (blocklistParts != null && blocklistParts.length > 1) {
        preparedStatement.setString(7, blocklistParts[1]);
      } else {
        preparedStatement.setString(7, "");
      }
      preparedStatement.setString(8, digest);
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }

  private void addPoolAssignments(BridgePoolAssignment desc, String digest,
      Connection conn) {
    try (
      PreparedStatement preparedStatement = conn.prepareStatement(
          INSERT_BRIDGE_POOL_ASSIGNMENTS_FILE_SQL);
    ) {
      preparedStatement.setTimestamp(1,
          new Timestamp(desc.getPublishedMillis()));
      /*
       * XXX: We can hardcode the header for now, but it would be better to add
       * the parsing logic to metrics lib in case this changes.
       **/
      preparedStatement.setString(2, "@type bridge-pool-assignment 1.0");
      preparedStatement.setString(3, digest);
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
  }
}
