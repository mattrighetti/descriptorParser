package org.torproject.metrics.descriptorparser.builders;

import org.torproject.descriptor.BridgeServerDescriptor;
import org.torproject.descriptor.RelayServerDescriptor;
import org.torproject.descriptor.ServerDescriptor;
import org.torproject.metrics.descriptorparser.utils.DateTimeHelper;
import org.torproject.metrics.descriptorparser.utils.DescriptorUtils;
import org.torproject.metrics.descriptorparser.utils.LookupResult;
import org.torproject.metrics.descriptorparser.utils.LookupService;
import org.torproject.metrics.descriptorparser.utils.ReverseDomainNameResolver;
import org.torproject.metrics.descriptorparser.utils.TorVersion;
import org.torproject.metrics.descriptorparser.utils.TorVersionStatus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeSet;

public class RouterStatusBuilder {
  private static final Logger logger = LoggerFactory.getLogger(
      RouterStatusBuilder.class);

  private static final String INSERT_ROUTER_STATUS_SQL
      = "INSERT INTO server_status"
      + " (is_bridge, published, nickname, fingerprint,"
      + " or_addresses, last_seen, first_seen, running, flags,"
      + " country, country_name, autonomous_system, as_name,"
      + " verified_host_names, last_restarted,"
      + " exit_policy, contacts, platform,"
      + " version, version_status, effective_family, declared_family,"
      + " transport, bridgedb_distributor, blocklist) VALUES"
      + "(?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?,"
      + " ?, ?, ?, ?, ?, ?, ?, ?, ?) ON CONFLICT DO NOTHING;";

  private static final String SELECT_LAST_ROUTER_STATUS_SQL
      = "SELECT * FROM server_status WHERE fingerprint=?"
      + " AND nickname=? AND published<=? ORDER BY published DESC LIMIT 1;";

  private static final String SELECT_FIRST_NETWORK_STATUS_SQL
      = "SELECT * FROM network_status_entry WHERE fingerprint=?"
      + " AND nickname=? AND time<=? ORDER BY time ASC LIMIT 1;";

  private static final String SELECT_LAST_NETWORK_STATUS_SQL
      = "SELECT * FROM network_status_entry WHERE fingerprint=?"
      + " AND nickname=? AND time<=? ORDER BY time DESC LIMIT 1;";

  private static final String SELECT_FIRST_BRIDGE_STATUS_SQL
      = "SELECT * FROM bridge_status WHERE fingerprint=?"
      + " AND nickname=? AND published<=? ORDER BY published ASC LIMIT 1;";

  private static final String SELECT_LAST_BRIDGE_STATUS_SQL
      = "SELECT * FROM bridge_status WHERE fingerprint=?"
      + " AND nickname=? and published<=? ORDER BY published DESC LIMIT 1;";

  private static final String SELECT_LAST_BRIDGESTRAP_TEST_SQL
      = "SELECT * FROM bridgestrap_test WHERE fingerprint=?"
      + " AND published<=? ORDER BY published DESC LIMIT 1;";

  private static final String SELECT_LAST_BRIDGE_POOL_SQL
      = "SELECT * FROM bridge_pool_assignment WHERE fingerprint=?"
      + " AND published<=? ORDER BY published DESC LIMIT 1;";

  private static final String SELECT_NETWORK_STATUS_SQL
      = "SELECT * FROM network_status WHERE digest=?;";

  private static final String SELECT_LAST_NETWORK_STATUS_TIME_SQL
      = "SELECT * FROM network_status WHERE valid_after<=? ORDER BY"
      + " valid_after DESC LIMIT 1;";

  private static final String SELECT_FAMILY_MEMBERS
      = "SELECT * FROM server_status WHERE fingerprint=? ORDER BY published "
      + "DESC LIMIT 1;";

  private static boolean isBridge = false;

  private ReverseDomainNameResolver reverseDomainNameResolver;

  private LookupService lookupService;

  private DescriptorUtils descUtils = new DescriptorUtils();

  private long now;

  private long firstSeenMillis = 0L;
  private long lastSeenMillis = 0L;

  private String version = null;
  private String[] recommendedVersions = null;
  private String networkStatus = null;

  private String transports = "";
  private String distributionMethod = "";
  private String blocklist = "";

  private long bridgestrapLastEndMillis = -1L;

  private String flags = "";

  private SortedMap<String, Integer> lastBandwidthWeights = null;

  private SortedSet<TorVersion> lastRecommendedServerVersions = null;

  private SortedSet<String> declaredFamily = new TreeSet<>();
  private SortedSet<String> effectiveFamily = new TreeSet<>();

  /**
   * Add a record to the server_status table.
   * A record represents the current status of a network node.
   */
  public void build(ServerDescriptor desc, Connection conn) throws Exception {

    String selectLastStatus = "";
    String selectFirstStatus = "";

    this.now = System.currentTimeMillis();
    this.reverseDomainNameResolver = new ReverseDomainNameResolver();
    this.lookupService = new LookupService(new File("geoip"));

    if (desc instanceof RelayServerDescriptor) {
      desc = (RelayServerDescriptor) desc;
      this.isBridge = false;
      selectLastStatus = SELECT_LAST_NETWORK_STATUS_SQL;
      selectFirstStatus = SELECT_FIRST_NETWORK_STATUS_SQL;
      this.effectiveFamily.add(desc.getFingerprint());
      if (desc.getFamilyEntries() != null) {
        for (String familyMember : desc.getFamilyEntries()) {
          if (familyMember.startsWith("$") && familyMember.length() == 41) {
            familyMember = familyMember.substring(1, 41).toUpperCase();
          }
          declaredFamily.add(familyMember);
          PreparedStatement preparedFamilyQuerySt = conn.prepareStatement(
              SELECT_FAMILY_MEMBERS);
          preparedFamilyQuerySt.clearParameters();
          preparedFamilyQuerySt.setString(1, familyMember);
          try {
            ResultSet rs = preparedFamilyQuerySt.executeQuery();
            if (rs.next()) {
              String member = rs.getString("fingerprint");
              if (member.startsWith("$") && member.length() == 41) {
                member = member.substring(1, 41).toUpperCase();
              }
              String declaredFam = rs.getString("declared_family");
              if (declaredFam.contains(desc.getFingerprint())) {
                this.effectiveFamily.add(member);
              }
            }
          } catch (SQLException ex) {
            logger.warn("Exception. {}".format(ex.getMessage()));
          }
        }
      }
      this.declaredFamily.add(desc.getFingerprint());
    } else {
      desc = (BridgeServerDescriptor) desc;
      selectLastStatus = SELECT_LAST_BRIDGE_STATUS_SQL;
      selectFirstStatus = SELECT_FIRST_BRIDGE_STATUS_SQL;
      this.isBridge = true;
    }

    PreparedStatement preparedQueryStatement = conn.prepareStatement(
        SELECT_LAST_ROUTER_STATUS_SQL);
    preparedQueryStatement.clearParameters();
    preparedQueryStatement.setString(1, desc.getFingerprint());
    preparedQueryStatement.setString(2, desc.getNickname());
    preparedQueryStatement.setTimestamp(3,
        new Timestamp(desc.getPublishedMillis()));

    try {
      ResultSet rs = preparedQueryStatement.executeQuery();
      if (rs.next()) {
        this.firstSeenMillis = rs.getTimestamp("first_seen").getTime();
        preparedQueryStatement = conn.prepareStatement(
            selectLastStatus);
        preparedQueryStatement.clearParameters();
        preparedQueryStatement.setString(1, desc.getFingerprint());
        preparedQueryStatement.setString(2, desc.getNickname());
        preparedQueryStatement.setTimestamp(3,
            new Timestamp(desc.getPublishedMillis()));
        rs = preparedQueryStatement.executeQuery();
        if (rs.next()) {
          this.flags = rs.getString("flags");
          if (this.isBridge) {
            this.lastSeenMillis = rs.getTimestamp("published").getTime();
            this.version = this.getBridgeServerVersion(desc);
          } else {
            this.lastSeenMillis = rs.getTimestamp("time").getTime();
            this.version = rs.getString("version");
          }
          this.networkStatus = rs.getString("network_status");
        }
      } else {
        preparedQueryStatement = conn.prepareStatement(
            selectFirstStatus);
        preparedQueryStatement.clearParameters();
        preparedQueryStatement.setString(1, desc.getFingerprint());
        preparedQueryStatement.setString(2, desc.getNickname());
        preparedQueryStatement.setTimestamp(3,
            new Timestamp(desc.getPublishedMillis()));
        rs = preparedQueryStatement.executeQuery();
        if (rs.next()) {
          if (this.isBridge) {
            this.firstSeenMillis = rs.getTimestamp("published").getTime();
            this.version = this.getBridgeServerVersion(desc);
          } else {
            this.firstSeenMillis = rs.getTimestamp("time").getTime();
            this.version = rs.getString("version");
          }
          this.lastSeenMillis = this.firstSeenMillis;
        } else {
          /**
           * Return if the router has not been included in a consensus document
           * just yet.
           */
          return;
        }
      }
    } catch (SQLException ex) {
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
    if ((desc instanceof RelayServerDescriptor)
        && (this.networkStatus != null)) {
      try {
        preparedQueryStatement = conn.prepareStatement(
            SELECT_NETWORK_STATUS_SQL);
        preparedQueryStatement.clearParameters();
        preparedQueryStatement.setString(1, this.networkStatus);
        ResultSet rs = preparedQueryStatement.executeQuery();
        if (rs.next()) {
          String versions = rs.getString("recommended_server_version");
          this.recommendedVersions = this.getRecommendedVersions(versions);
        }
      } catch (SQLException ex) {
        logger.warn("Exception. {}".format(ex.getMessage()));
      }
    } else {
      try {
        preparedQueryStatement = conn.prepareStatement(
          SELECT_LAST_NETWORK_STATUS_TIME_SQL);
        preparedQueryStatement.clearParameters();
        preparedQueryStatement.setTimestamp(1,
            new Timestamp(desc.getPublishedMillis()));
        ResultSet rs = preparedQueryStatement.executeQuery();
        if (rs.next()) {
          String versions = rs.getString("recommended_server_version");
          this.recommendedVersions = this.getRecommendedVersions(versions);
        }
      } catch (SQLException ex) {
        logger.warn("Exception. {}".format(ex.getMessage()));
      }
    }
    boolean running = true;
    if (!this.isBridge && this.lastSeenMillis
        >= (now - DateTimeHelper.ONE_WEEK)) {
      running = false;
    } else if (this.isBridge) {
      try {
        preparedQueryStatement = conn.prepareStatement(
            SELECT_LAST_BRIDGESTRAP_TEST_SQL);
        preparedQueryStatement.clearParameters();
        preparedQueryStatement.setString(1, desc.getFingerprint());
        preparedQueryStatement.setTimestamp(2,
            new Timestamp(desc.getPublishedMillis()));
        ResultSet rs = preparedQueryStatement.executeQuery();
        if (rs.next()) {
          boolean result = rs.getBoolean("result");
          long bridgestrapLastEndMillis
              = rs.getTimestamp("published").getTime();
          if (!result) {
            running = false;
          } else if (bridgestrapLastEndMillis
              >= (now - DateTimeHelper.ONE_WEEK)) {
            running = false;
          }
        } else {
          running = false;
        }
      } catch (SQLException ex) {
        logger.warn("Exception. {}".format(ex.getMessage()));
      }
      try {
        preparedQueryStatement = conn.prepareStatement(
            SELECT_LAST_BRIDGE_POOL_SQL);
        preparedQueryStatement.clearParameters();
        preparedQueryStatement.setString(1, desc.getFingerprint());
        preparedQueryStatement.setTimestamp(2,
            new Timestamp(desc.getPublishedMillis()));
        ResultSet rs = preparedQueryStatement.executeQuery();
        if (rs.next()) {
          this.transports = rs.getString("transports");
          this.distributionMethod = rs.getString("distribution_method");
          this.blocklist = rs.getString("blocklist");
        }
      } catch (SQLException ex) {
        logger.warn("Exception. {}".format(ex.getMessage()));
      }
    }

    SortedSet<String> addressStrings = new TreeSet<>();
    addressStrings.add(desc.getAddress());
    SortedMap<String, LookupResult> lookupResults =
        this.lookupService.lookup(addressStrings);
    LookupResult lookupResult = lookupResults.get(desc.getAddress());
    Map<String, Long> addressLastLookupTimes = new HashMap<>();
    addressLastLookupTimes.put(desc.getAddress(), lastSeenMillis);
    this.reverseDomainNameResolver.setAddresses(addressLastLookupTimes);
    this.reverseDomainNameResolver.startReverseDomainNameLookups();
    Map<String, SortedSet<String>> verifiedLookupResults =
        this.reverseDomainNameResolver.getVerifiedLookupResults();

    SortedSet<String> verifiedHostNames =
        verifiedLookupResults.get(desc.getAddress());

    try (
      PreparedStatement preparedStatement =
          conn.prepareStatement(INSERT_ROUTER_STATUS_SQL);
    ) {
      preparedStatement.setBoolean(1, this.isBridge);
      preparedStatement.setTimestamp(2,
          new Timestamp(desc.getPublishedMillis()));
      preparedStatement.setString(3, desc.getNickname());
      preparedStatement.setString(4, desc.getFingerprint());
      preparedStatement.setString(5,
          descUtils.fieldAsString(desc.getOrAddresses()));
      preparedStatement.setTimestamp(6,
          new Timestamp(lastSeenMillis));
      preparedStatement.setTimestamp(7,
          new Timestamp(firstSeenMillis));
      preparedStatement.setBoolean(8, running);
      preparedStatement.setString(9, this.flags);
      if (lookupResult != null) {
        preparedStatement.setString(10, lookupResult.getCountryCode());
        preparedStatement.setString(11, lookupResult.getCountryName());
        preparedStatement.setString(12, lookupResult.getAsNumber());
        preparedStatement.setString(13, lookupResult.getAsName());
      } else {
        preparedStatement.setString(10, "");
        preparedStatement.setString(11, "");
        preparedStatement.setString(12, "");
        preparedStatement.setString(13, "");
      }
      preparedStatement.setString(14, descUtils.fieldAsString(
          verifiedHostNames));
      preparedStatement.setTimestamp(15,
          new Timestamp(this.calculateLastRestartedMillis(desc)));
      preparedStatement.setString(16,
          descUtils.fieldAsString(desc.getExitPolicyLines()));
      if (desc.getContact() != null) {
        preparedStatement.setString(17, desc.getContact());
      } else {
        preparedStatement.setString(17, "");
      }
      preparedStatement.setString(18, desc.getPlatform());
      if (this.version != null) {
        preparedStatement.setString(19, this.version);
        preparedStatement.setString(20, this.getVersionStatus());
      } else {
        preparedStatement.setString(19, "");
        preparedStatement.setString(20, "");
      }
      if (desc.getFamilyEntries() != null) {
        preparedStatement.setString(21,
            descUtils.fieldAsString(this.effectiveFamily));
        preparedStatement.setString(22,
            descUtils.fieldAsString(desc.getFamilyEntries()));
      } else {
        preparedStatement.setString(21, desc.getFingerprint());
        preparedStatement.setString(22, desc.getFingerprint());
      }
      preparedStatement.setString(23, this.transports);
      preparedStatement.setString(24, this.distributionMethod);
      preparedStatement.setString(25, this.blocklist);
      preparedStatement.executeUpdate();
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.warn("Exception. {}".format(ex.getMessage()));
    }
    return;
  }

  private String getBridgeServerVersion(ServerDescriptor desc) {
    if (desc.getPlatform() != null) {
      String[] platformParts =
        desc.getPlatform().split(" on ");
      return platformParts[0].toLowerCase();
    }
    return null;
  }

  private String[] getRecommendedVersions(String versions) {
    if (versions != null) {
      return versions.split(" ")[1].split(",");
    }
    return null;
  }

  private Long calculateLastRestartedMillis(ServerDescriptor descriptor) {
    Long lastRestartedMillis = 0L;
    if (null != descriptor.getUptime()) {
      lastRestartedMillis = descriptor.getPublishedMillis()
          - descriptor.getUptime() * DateTimeHelper.ONE_SECOND;
    }
    return lastRestartedMillis;
  }

  private String getVersionStatus() {
    String stringTorVersion = null;
    /* Extract tor software version  */
    if (this.version != null
        && this.version.startsWith("Tor ")) {
      stringTorVersion = this.version.split(" ")[1];
    }

    TorVersion torVersion = TorVersion.of(stringTorVersion);
    String versionStatus = "";
    SortedSet<TorVersion> lastRecommendedServerVersions = new TreeSet<>();
    if (this.recommendedVersions != null) {
      for (String recommendedServerVersion :
          this.recommendedVersions) {
        TorVersion recommendedTorServerVersion
            = TorVersion.of(recommendedServerVersion);
        if (null != recommendedTorServerVersion) {
          lastRecommendedServerVersions.add(recommendedTorServerVersion);
        }
      }
    }

    versionStatus = (null != torVersion
        ? torVersion.determineVersionStatus(
            lastRecommendedServerVersions)
        : TorVersionStatus.UNRECOMMENDED).toString();
    return versionStatus;
  }
}
