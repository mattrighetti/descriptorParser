package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class BridgePoolAssignmentsParserTest {

  @Test()
  public void testBridgePoolAssignmentsParserDbUploader() throws Exception {
    BridgePoolAssignmentsParser bp = new BridgePoolAssignmentsParser();
    String bridgePoolAssignmentsPath =
        "src/test/resources/2022-08-29-23-47-57";
    String confFile = "src/test/resources/config.properties.test";
    String bridgePoolDigest = "9hxKhoktOK8F4GsCo6+jkug/y1HudnNI6FCKXaKrLmU";


    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    bp.run(bridgePoolAssignmentsPath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM bridge_pool_assignments_file WHERE digest = '"
        + bridgePoolDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("digest"), bridgePoolDigest);
        assertEquals(rs.getString("header"),
            "@type bridge-pool-assignment 1.0");
      } else {
        fail("Descriptor not found");
      }
    }
  }

}
