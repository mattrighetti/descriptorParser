package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class BandwidthParserTest {

  @Test()
  public void testBandwidthParserDbUploader() throws Exception {
    BandwidthParser bp = new BandwidthParser();
    String bandwidthPath =
        "src/test/resources/2022-08-29-10-39-44-bandwidth";
    String confFile = "src/test/resources/config.properties.test";
    String bandwidthFileDigest = "5Z/dxXZMPi7sUbeN951Ti0UUoRHnHdqqKIKK98OXsBQ";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    bp.run(bandwidthPath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM bandwidth_file WHERE digest = '"
        + bandwidthFileDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("digest"), bandwidthFileDigest);
        assertEquals(rs.getString("destination_countries"), "DE");
      } else {
        fail("Descriptor not found");
      }
    }
  }

}
