package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class BridgedbMetricsParserTest {

  @Test()
  public void testBridgedbMetricsParserDbUploader() throws Exception {
    BridgedbMetricsParser bp = new BridgedbMetricsParser();
    String bridgedbMetricsPath =
        "src/test/resources/2022-08-30-08-40-33-bridgedb-metrics";
    String confFile = "src/test/resources/config.properties.test";
    String bridgedbMetricsFileDigest =
        "/+OQEHFk0enrXjfGN4fRijYFZ7Lg5aTyE/bpHPEv5wU";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    bp.run(bridgedbMetricsPath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM bridgedb_metrics WHERE digest = '"
        + bridgedbMetricsFileDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("digest"), bridgedbMetricsFileDigest);
        assertEquals(rs.getString("version"), "2");
      } else {
        fail("Descriptor not found");
      }
    }
  }

}
