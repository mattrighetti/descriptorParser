package org.torproject.metrics.descriptorparser.parsers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.torproject.metrics.descriptorparser.utils.PsqlConnector;

import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class BridgestrapParserTest {

  @Test()
  public void testBridgestrapParserDbUploader() throws Exception {
    BridgestrapParser bp = new BridgestrapParser();
    String bridgestrapPath =
        "src/test/resources/2022-08-29-19-22-41-bridgestrap-stats";
    String confFile = "src/test/resources/config.properties.test";
    String bridgestrapDigest = "t7wwF8AzrJ78IwcHEr1qvsyP7z9r+vzb4LsauJBbZ30";

    Connection conn = null;
    PsqlConnector psqlConn = new PsqlConnector();
    conn = psqlConn.connect(confFile);

    bp.run(bridgestrapPath, conn);

    PreparedStatement preparedStatement = conn.prepareStatement(
        "SELECT * FROM bridgestrap_stats WHERE digest = '"
        + bridgestrapDigest + "'");

    try (ResultSet rs = preparedStatement.executeQuery()) {
      if (rs.next()) {
        assertEquals(rs.getString("digest"), bridgestrapDigest);
        assertEquals(rs.getString("header"),
            "@type bridgestrap-stats 1.0");
      } else {
        fail("Descriptor not found");
      }
    }
  }

}
